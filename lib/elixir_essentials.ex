defmodule ElixirEssentials do
  @moduledoc """
  Documentation for ElixirEssentials.
  """

  @doc """
  Hello world.

  ## Examples

      iex> ElixirEssentials.hello()
      :world

  """
  def hello do
    :world
  end
end
