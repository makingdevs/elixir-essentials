defmodule ElixirEssentials.MixProject do
  use Mix.Project

	@version "0.1.0"

  def project do
    [
      app: :elixir_essentials,
      version: @version,
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      deps: deps(),

			# Docs
			name: "Elixir Essentials",
			source_url: "https://gitlab.com/makingdevs/elixir-essentials",
			homepage_url: "http://makingdevs.com",
			docs: docs()
		]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:ex_doc, "~> 0.21", only: :dev, runtime: false},
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"},
    ]
  end

	defp docs do
		[
			main: "overview", # The main page in the docs
			logo: "logo.png",
			source_ref: "v#{@version}",
			logo: "guides/images/logo.png",
			extra_section: "GUIDES",
			source_url: "https://gitlab.com/makingdevs/elixir-essentials",
			extras: [
				"guides/overview.md",
				"guides/thanks.md",
				"guides/introduction/installation.md",
				"guides/concepts/0_introduction.md",
				"guides/concepts/1_data_types.md",
				"guides/concepts/2_build_tools.md",
				"guides/concepts/3_pattern_matching.md",
				"guides/concepts/4_functions_flow_control.md",
				"guides/concepts/5_functional_programming.md",
				"guides/concepts/6_collections.md",
				"guides/concepts/7_processes.md",
			],
			groups_for_extras: [
				"Introducción": ~r/guides\/introduction\/.?/,
				"Guias": ~r/guides\/[^\/]+\.md/,
				"Entrenamiento": ~r/training\/[^\/]+\.md/,
				"Conceptos": ~r/concepts\/[^\/]+\.md/,
			],
		]
	end
end
