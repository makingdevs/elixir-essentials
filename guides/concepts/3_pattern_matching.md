# Pattern Matching

Ahora que ya estamos familiarizados con los bloques de construcción básicos de Elixir, es tiempo de ver algunos elementos típicos, y cómo verás se trabaja diferente a diferencia de la mayoría de los lenguajes modernos imperativos.

Importante: Construcciones de condicionales clásicas como `if` y `case` son a menudo reemplazadas fon funciones **multi-clausula**, y que no hay sentencias clásicas de ciclo como `while`. Pero aún podemos resolver problemas de complejidad arbitaria en Elixir, y el código resultante no es más complicado que una solución típica Orientada a Objetos.

## El operador `=`

Este operador **NO es de asignación**, en lugar de ello cuando escribimos `a = 1` se dice qie atamos `a` al valor de `1`.

El operador *`=`* es llamado **match operator**(operador de coincidencia), y la expresión parecida a la asignación es un ejemplo de pattern matching.

### Usado para atado (asignación)

La _asignación_ de variables sólo pasa cuando la variable está del lado izquierdo de la expresión.

```elixir
programmers = %{joe: "Erlang", matz: "Ruby", rich: "Clojure"}
Map.put(programmers, :jose, "Elixir")
programmers
programmers = Map.put(programmers, :jose, "Elixir")
programmers
```

### Usado para coincidir

Intentemos invertir la expresión:

```elixir
%{joe: "Erlang", jose: "Elixir", matz: "Ruby", rich: "Clojure"} = programmers
```

Nótese como no estamos asignando, en lugar de ello, una **coincidencia de patrón** ha ocurrido. debido a que el contenido del lado izquierdo y derecho es el mismo.

Cuando un patrón no coincide entonces un error del tipo `MatchError` es alcanzdo.

```elixir
%{foo: "bar"} = programmers
** (MatchError) no match of right hand side value: %{joe: "Erlang", jose: "Elixir", matz: "Ruby", rich: "Clojure"}
```

Este tipo de situaciones sirve como aseveraciones dentro del código, y nos serán útiles para destructurar.

### Usado para destructuración

Destructuración es el punto en donde la coincidencia de patrones realmente es brillante. Una buena definición la podemos encontrar desde Common LiSP:

> La desestructuración permite vincular un conjunto de variables a un conjunto de valores correspondiente en cualquier lugar donde normalmente puede vincular un valor a una sola variable.

```elixir
%{joe: a, jose: b, matz: c, rich: d} = %{joe: "Erlang", jose: "Elixir", matz: "Ruby", rich: "Clojure"}
a # "Erlang"
b # "Elixir"
c # "Ruby"
d # "Clojure"
```

Aquí, atamos un conjunto de variables(a, b, c, d) a sus valores correspondientes. Pero a veces querremos empatar algunas partes del patrón, no el patrón completo, esto se puede hacer sin específicar la coincidencia completa.

```elixir
%{jose: the_language} = %{joe: "Erlang", jose: "Elixir", matz: "Ruby", rich: "Clojure"}
the_language # Elixir
{date, time} = :calendar.local_time()
```

#### Coincidencias en listas

Hay una serie de elementos que deberías saber con respecto a los patrones en las listas. Las listas en Elixir son un tipo de listas enlazadas, y eso permite que su complejidad sea una operación linear **O(n)**. En breve una definición de lista sería:

> Una lista no vacía consiste en una cabeza y una cola. Dónde la cola es una lista.


```elixir
list = [1,2,3,4]
[1 | [2 | [3 | [4 | []]]]] = list
[head | tail] = list
head # 1
tail # [1,2,3]
[a, _ | rest] = tail
rest # [4]
[h | t] = rest
[1,2,3] ++ [5,6,7]
```

#### Ignorando valores con `_`

Si no necesitamos capturar un valor durante la coincidencia, podríamos usar el vartiable especial `_`.

```elixir
[1, _, _] = [1, 2, 3]
[1, _, _] = [1, "cat", "dog"]
{_date, time} = :calendar.local_time()
{_, {hour, _, _}} = :calendar.local_time()
```

#### Las variables se atan una vez por coinciencia

Una vez que una variable se ha vinculado a un valor en el proceso de coincidencia, conserva ese valor por el resto de la coincidencia.


```elixir
[a, a] = [1, 1]
[b, b] = [1, 2] # MatchError
```

Sin embargo una variable puede ser vínculada a un nuevo valor en una coincidencia subsecuente , el valor actual no participa en la nueva coincidencia.

```elixir
a = 1
[1, a, 3] = [1, 2, 3]
a # 2
```

Ocasionalmente necesitarás coincidir contra el valor de una variable. Para este propósito --- Pin operator ^