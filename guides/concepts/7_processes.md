# Manejo de procesos

En Elixir todo el código corre dentro de procesos, los cuales, están aislados unos de otros, corren de manera concurrente y se comunican con el paso de mensajes. Los procesos no son sólo la base de la concurrencia en Elixir, además proveen la forma de construir programas distribuidos y tolerantes a fallos.

Los procesos de Elixir no deberían ser confundidos con procesos del sistema operativo, ya que los procesos de Elixir son extremadamente ligeros en términos de memoria y CPU, incluso comparados con hilos usados en otros lenguajes de programación. Debido a esto, es común tener decenas de cientos de de procesos corriendo simúltáneamente.

![alt beam](https://3.bp.blogspot.com/-VJw-NpEbVyE/WMexqAlhxNI/AAAAAAAAFsA/qhpPIqfUKGIUIdpNGEqjIuki8inPGoA_gCLcB/s1600/erlang%2Bscheduler.png)

## Creación de Procesos

Es muy fácil crear un nuevo proceso:

```elixir
spawn fn ->
  IO.puts "Esto corre enm un proceso separadao"
end

spawn AModule, :some_function, [args]
```

![alt process](https://learnyousomeerlang.com/static/img/link-exit.png)

## Envío y recepción de mensajes

Cuándo creas un proceso obtienes un Identificador de Proceso o PID

```elixir
pid = spawn(fn -> ... end)

self() # Obtienes el PID del proceso actual

# Para enviar un mensaje al proceso puedes ocupar `send`
send pid, :message
```

![alt mailbox](https://learnyousomeerlang.com/static/img/hello.png)

### Recepción de mensajes

```elixir
receive do
  message -> # maneja el mensaje
end
```

La macro `receive` soporta Pattern Matching:

```elixir
receive do
  {:compute, caller, n} ->
    result = Factorial.of(n)
    send caller, {:result, result}
  {:status, msg} -> Logger.debug(msg)
    _ -> # Comportamiento por defecto
end
```

Si por alguna razón, deseas establecer un timeout para un proceso puedes hacerlo con `after`:

```elixir
receive do
  {:compute, caller, n} ->
    result = Factorial.of(n)
    send caller, {:result, result}
  {:status, msg} -> Logger.debug(msg)
    _ -> # Comportamiento por defecto
  after 500 -> # se acabó...
end
```

Lo que realmente estarás haciendo serán procesos de funciones definidas dentro de módulos, en donde definirás las acciones a realizar cuando recibas un mensaje.

```elixir
defmodule Hello do
  def greet do
    receive do
      {:greet, message} ->
        IO.puts "Hello #{message}"
        greet
      _ ->
        greet() # Sin acción alguna
    end
  end
end

pid = spawn(Hellom, :greet, [])
```

El envío de mensajes lo haces con ayuda de la macro `send`:

```elixir
send pid, {:greet, "MakingDevs"}
# Imprime "Hello MakingDevs"
send pid, {:greet, "Juan"}
# Imprime "Hello Juan"
```

Para matar un proceso(con fines didácticos) puedes usar la función `Process.exit/2`, indicando una razón de finalización del proceso. [Puedes revisar su documentación](https://hexdocs.pm/elixir/Process.html#exit/2).

```elixir
pid = spawn(fn -> IO.puts("Hello world") end)
Process.exit(pid, :kill) # => true
```

## Vínculación

Para atar dos procesos tienes que usar `spawn_link`:

```elixir
me = self()
you = spawn_link(fn -> receive do _ -> :nil end end)
Process.exit(you, :kill)
# ** (EXIT from #PID<0.102.0>) shell process exited with reason: killed
```

Si uno de los proceso vínculados muere, entonces el proceso que lo creo también muere.

## Manejo de errores

Cuando un proceso termina, siempre el proceso en cuestión manda un último mensaje indicando su terminación, dicho mensaje puede ser manejado por el proceso vínculado:

```elixir
# Evita que el proceso actual termine por un proceso vínculado que termina también
Process.flag(:trap_exit, true)
me = self()
you = spawn_link(fn -> receive do _ -> :nil end end)
Process.exit(you, :kill)
flush()
# {:EXIT, #PID<0.105.0>, :killed}
```

Puedes recibir el error en el proceso vínculado con el patrón de la tupla:

```elixir
receive do
  {:EXIT, pid, reason} -> # Manejo del mensaje de terminación
end
```

## Monitor de procesos

Puedes observar y monitorear los proceso creados con `spawn_monitor`, sin embargo el valor obtenido por la función es distinto:

```elixir
{you, _ref} = spawn_monitor(fn -> receive do _ -> :nil end end)

# Puedes coincidir el mensaje de terminación
receive do
  {:DOWN, _ref, :process, pid, reason} -> # Revivirás el proceso???
end
# {:DOWN, #Reference<0.2740174413.3442999298.207230>, :process, #PID<0.104.0>, :killed}
```

## ¿Cómo usar los procesos?

```elixir
defmodule Parallel do
  def pmap(collection, fun) do
    collection
    |> Enum.map(&spawn_process(&1, self, fun))
    |> Enum.map(&await/1)
  end

  defp spawn_process(item, parent, fun) do
    spawn_link fn ->
      send parent, {self, fun.(item)}
    end
  end

  defp await(pid) do
    receive do
      {^pid, result} -> result
    end
  end
end
```

**¿Cómo mejoras el rendimiento de la función `Parallel.pmap/2`?**

> TODO: Crea un proceso de larga duración y envíale miles de mensajes

> TODO: Haz un ping pong de procesos

> TODO: Víncula _n_ cantidad de procesos de forma linear y después destrúyelos

