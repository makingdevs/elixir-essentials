# Funciones y flujo de control

Elixir es un lenguaje funcional, no es de sorprenderse que los ciudadanos de primer nivel sean las funciones. Las funciones generalmente son parte de un módulo, aunque también podemos declararlas fuera de ellos.

Los nombres de las funciones siguen las mismas convenciones como las variables. Y a menudo se usan los caracteres `?` y `!` en sus declaraciones. El cáracter `?` se usa en las funciones que regresan valores como `true` o `false`, y el carácter `!` al final del nombre de la función indica que posiblemente arroje un error en tiempo de ejecución, ambas son convenciones más que reglas, pero son informativas para los programadores, así que, también podrías declarar tus funciones de la misma manera.

> En definitiva las funciones son elemento principal para el uso de procesos dentro de la plataforma.

## Anónimas

Una función anónima usa la palabra reservada `fn` en un bloque:

```elixir
fn
  param_list ... -> body
  param_list ... -> body ...
end
```

Piensa en `fn` y `end` como un String multilínea que rodea el contenido, sólo que aqui lo que se declara es una función. Con esto podemos usarlo cómo argumento en la llamada a otra función o invocarlo.

```elixir
sum = fn (a, b) -> a + b end
sum.(1, 2)

say_hi = fn -> IO.puts("Hello MakingDevs!") end
say_hi.()

multiply = fn a, b -> a * b end # Puedes omitir los paréntesis
multiply.(5, 6)

handle_open = fn
  {:ok, file} -> "Read data: #{IO.read(file, :line)}"
  {_, error} -> "Error: #{:file.format_error(error)}"
end
handle_open.(File.open("code/intro/hello.exs"))
```

El valor de retorno de cualquier función es la última expresión, no hay un `return` explícito. La recomendación es mantener las funciones breves y simples. lo cual hace más fácil calcular el resultado y observar la última expresión.

## Operador de captura `&`

Elixir provee una forma corta para la definición de funciones anónimas usando el operador `&`(operador de captura). En lugar de usar `fn` se usa `&`, rodeado de paréntesis, y en lugar de nombrar los parámetros los enumeras, por ejemplo &1, &2, etc.

Por ejemplo puedes escribir `&(&1 + 1)` en lugar de `fn x -> x + 1 end`.

```elixir
fall_velocity = fn (distance) -> :math.sqrt(2 * 9.8 * distance) end # Forma convencional
fall_velocity.(1)

fall_velocity = &(:math.sqrt(2 * 9.8 * &1)) # Forma corta
fall_velocity.(10)
```

## Módulos

La mayoría de los programas en Elixir(excepto los de pequeños cálculos) definen sus funciones en módulos compilados en lugar de una shell, o un script. Son el lugar formal para poner los programas, tu código. Lo interesante es la forma cómo tratan tu código; ahora debes de considerar que Elixir, o Erlang trata distinto el código pues almacena, encapsula, comparte y lo administra de forma eficiente.

Cada módulo debe estar en su propio archivo, con una extensión `.ex`(realmente puedes poner más de un módulo pero conservalo simple).

```elixir
defmodule Math do
  def sum(a, b) do
    a + b
  end
end

defmodule Geometry do
  def rectangle_area(a, b), do: a * b # Short form
end
```

Para compilarlo puedes usar lo siguiente:

- `elixirc math.ex` y después usarlo con la `iex`
- Puedes usar `elixir math.ex` y eso compilará y ejecutará el archivo, inclusive podrías poner un bloque de script por debajo.

Los módulos proveen 'namespaces' para las cosas que definimos. Ya hemos visto como encapsulan funciones, pero además actuán como envolturas para macros, structs, protocolos y otros módulos. Como se ha visto si quieres usar una función fuera del módulo entonces tenemos que prefijar con el nombre del módulo.

Se pueden anidar los módulos para tener una estructura u organización de la semántica de las funciones.

```elixir
defmodule Outer do
  defmodule Inner do
    def inner_func do
    end

    def outer_func do
      Inner.inner_func
    end
  end
end

Outer.outer_func
Outer.Inner.inner_func
```

Sin embargo el anidado de módulos es una ilusión, todos lo módulos están definidos desde el principio, cuando definimos un módulo dentro de otro Elixir simplemente anteponemos el nombre del módulo al nombre del módulo interno, poniendo un punto entre ambos, es decir:

```elixir
defmodule Mix.Tasks.Doctest do
  def run do
  end
end

Mix.Tasks.Doctest.run
```

Esto significa que no hay una relación particular entre módulos `Mix` y `Mix.Tasks.Doctest`.

> **NOTA** Puedes referencia a un módulo por si mismo dentro de su declaración con la sintaxis `__MODULE__`

### Funciones nombradas

Es la forma que tiene elixir a través de los módulos para estructurar el código, estas funciones tienen que estar dentro de los módulos.

En Elixir una función nombrada es identificada por propiamente la declaración y sú numero de parámetros, a esto se le conoce como *aridad*.

*Aridad* es un nombre lujoso para el número de argumentos que una función recibe. Una función es identificada de forma única por el móudlo que la contiene, su nombre, y la aridad.

```shell
iex(1)> String.
Break                   Casing                  Chars
Tokenizer               Unicode                 at/2
bag_distance/2          capitalize/1            capitalize/2
chunk/2                 codepoints/1            contains?/2
downcase/1              downcase/2              duplicate/2
ends_with?/2            equivalent?/2           first/1
graphemes/1             jaro_distance/2         last/1
length/1                match?/2                myers_difference/2
next_codepoint/1        next_grapheme/1         next_grapheme_size/1
normalize/2             pad_leading/2           pad_leading/3
pad_trailing/2          pad_trailing/3          printable?/1
printable?/2            replace/3               replace/4
replace_leading/3       replace_prefix/3        replace_suffix/3
replace_trailing/3      reverse/1               slice/2
slice/3                 split/1                 split/2
split/3                 split_at/2              splitter/2
splitter/3              starts_with?/2          to_atom/1
to_charlist/1           to_existing_atom/1      to_float/1
to_integer/1            to_integer/2            trim/1
trim/2                  trim_leading/1          trim_leading/2
trim_trailing/1         trim_trailing/2         upcase/1
upcase/2
```

```elixir
defmodule Greeter do
  def hello(), do: "Hello, anonymous person!"   # hello/0
  def hello(name), do: "Hello, " <> name        # hello/1
  def hello(name1, name2), do: "Hello, #{name1} and #{name2}" # hello/2
end

defmodule Rectangle do
  def area(a), do: area(a, a)
  def area(a, b), do: a * b
end
```

> ¿Cómo agregas a una función la aridad en dónde acepte una lista de nombres, un sólo nombre, o ninguno?

### Funciones privadas

Cuando no queremos que otros módulos vean estas funciones podemos usar `defp` para que sólo sea invocada dentro del mismo módulo dónde se está definiendo.

```elixir
defmodule Greeter do
  def hello(name), do: phrase() <> name
  defp phrase, do: "Howdy, "
end
```

## Directivas para módulos

Elixir cuenta con tres directivas que simplifican el trabajo con los módulos. Dichas son ejecutadas cuando tu programa corre, y el efecto de las tres es el _alcance léxico_ que comienza en el punto donde la directiva es encontrada, y para al final del alcance de la misma declaración. Esto significa que una directiva en la definición de un módulo toma efecto desde el lugar


## Pattern matching en funciones

Aplica para funciones nombradas y anónimas, la diferencia es que escribimos la función varias veces, cada una con sus propia lista de parámetros y su cuerpo, y aunque parezca la definición de múltiples funciones, realmente es la definición de la misma función.

Cuando llamas a una función, Elixir intenta coincidir(_match_) los argumentos con la lista de parámetros de la primer definición(clausula), si no coinciden entonces intenta la siguiente definición de la misma función, pra verificar si coincide, continua hasta que ya no hay más definiciones.

```elixir
# Módulos
defmodule Geometry do
  def area({:rectangle, a, b}) do
    a * b
  end
  def area({:square, a}) do
    a * a
  end
  def area({:circle, r}) do
    r * r * 3.14
  end
end

# Oneline function declaration
defmodule MeterToLengthConverter do
  def convert(:feet, m), do: m * 3.28084
  def convert(:inch, m), do: m * 39.3701
  def convert(:yard, m), do: m * 1.09361
end

# Cualquier Pattern Matching
defmodule Greeter do
  # def hello(%{name: person_name}) do
  def hello(%{name: person_name} = person) do
    IO.puts "Hello, " <> person_name
  end
end

# Funciones anónimas
area = fn
  {:rectangle, a, b} -> a * b
  {:square, a} -> a * a
  {:circle, r} -> r * r * 3.14
end
```

El pattern matching ayuda a estructurar funciones y de-estructurar las llamadas a dichas funciones, se pueden usar valores concretos, o incluso omitir algunos valores en el patrón(`_`).

```elixir
defmodule Factorial do
  def of(0), do: 1
  def of(n), do: n * of(n - 1)
end
```

> Intenta obtener el máximo común divisor con la definición que encuentras en la Wikipedia.

```elixir
defmodule MDList do
  def sum(list) do
    sum(list, 0)
  end

  defp sum([], n), do: n
  defp sum([h|t], n), do: sum(t, n + h)
end
```

> ¿Puedes hacer lo mismo para calcular la longitud de una lista?

> TODO: Intenta capturar la función de un módulo

## Argumentos por default

Podemos decirle a las funciones que ciertos parámetros tengan valores por defecto, por ejemplo:

```shell
iex(2)> File.open
open!/1    open!/2    open!/3    open/1     open/2     open/3
```

Esto significa que:

```elixir
File.open("file.txt")
File.open("file.txt", [:read, :write])
File.open("file.txt", [:read, :write], fn file ->
  IO.read(file, :line)
end)
```

Los dos argumentos ya están definidos por default dentro de la implementación de la función `File.open`.

¿Cómo haces esto?

```elixir
defmodule File do
  def open(path, permissions \\ [:read], operation \\ FileHandler) do
    # ...
  end
end
```

Esto último sólo fue para explicar la forma en como se lograría este comportamiento, sin embargo, podemos hacer algo más simple para explicarlo:

```elixir
defmodule Greeter do
  def hello(name, language_code \\ "en") do
    phrase(language_code) <> name
  end

  defp phrase("en"), do: "Hello, "
  defp phrase("es"), do: "Hola, "
end
```

Otro ejemplo podría ser:

```elixir
defmodule Concat do
  def join(a, b, sep \\ " ") do
    a <> sep <> b
  end
end

IO.puts Concat.join("Hello", "world")      #=> Hello world
IO.puts Concat.join("Hello", "world", "_") #=> Hello_world
```

Complementa los parámetros por defecto con las guardas.

## Guardas

Las *guardas* son extension del mecanismo básico de coincidencia de patrones. Permiten ampliar el estado adicional de la expectativa que deben ser satisfechas para la coincidencia de un patrón entero. Una guarda puede ser específicada proveyendo la clausula `when` después de la lista de argumentos.

```elixir
defmodule Math do
  def kind(n) when x < 0, do: :negative
  def kind(0), do: :zero
  def kind(n) when x > 0, do: :positive
end
```

Esta guarda es una expresión lógica que pone condiciones sobre la clausula. Y para este caso aún tenemos un problema, ¿qué pasa si llamamos `Math.kind(:md)`?, para cubrirlo complementamos la guarda.

```elixir
defmodule Math do
  def kind(n) when is_number(n) and x < 0, do: :negative
  def kind(0), do: :zero
  def kind(n) when is_number(n) and x > 0, do: :positive
end
```

Este código, usa la función `Kernel.is_number/1` para determinar que el argumento es un número. Te sugiero revisar la documentación del módulo [Kernel](https://hexdocs.pm/elixir/Kernel.html).

El conjunto de operadores y funciones que pueden ser llamados desde las guardas son muy límitados. En particular, no puedes llamar a tus propias funciones, y algunas otras funciones no funcionan. Estos serían algunos de los operadores y funciones permitidos en las guardas:

- Operadores de comparación (`==`, `!=`,`===`, `!==`, `>`, `<`, `<=`, `>=`)
- Operadores lógios y de negación (`and`, `or`, `not`, `!`)
- Operadores aritméticos (`+`, `-`, `*`, `/`)
- Funciones de chequeo de tipos del módulo `Kernel`

Existen diferentes partes donde las guardas pueden ser usadas, estas son:

- Clausulas de funciones
- Expresiones `case`
- Funciones anónimas
- Guardas personalizadas definidas con `defguard/1` y `defguard/1`, las cuales pueden se definidas basadas en las existentes.

> TODO: Define nuevamente algunas de las funciones anteriores con guardas.

## Pipe Operator `|>`

Existe una forma de combinar funciones: usando el operador pipe `|>`, el cual permite pasar una expresión al primer argumento de la siguiente función.

Por ejemplo, todos hemos visto código cómo este:

```elixir
path = "."
path_with_files = Path.join(path, "**/*.ex")
list_files = Path.wildcard(path_with_files)
files = Enum.filter(list_files, fn fname ->
  String.contains?(Path.basename(fname), "application")
end)
```

Puedes ser incluso re-escrita de la siguiente manera:

```elixir
files = Enum.filter(Path.wildcard(Path.join(".", "**/*.ex")), fn fname ->
  String.contains?(Path.basename(fname), "application")
end)
```

Este código puede ser _inteligente_ o abreviado, pero realmente no es legible, para lo cuál Elixir cuenta con una mejor forma de escribirlo:

```elixir
files = "."
  |> Path.join("**/*.ex")
  |> Path.wildcard()
  |> Enum.filter(fn fname -> String.contains?(Path.basename(fname), "application") end)
```

El operador `|>` toma el resultado de la expresión de lado derecho y la inserta cómo el primer parámetro de la invocación de la función de la derecha.

> `val |> f(a,b)` es básicamente lo mismo cómo llamar `f(val, a, b)`

*NOTA:* Siempre deberías de usar paréntesis alrededor de los parámetros cuando usas los pipelines.

El aspecto clave del operador es que te deja escribir código que sigue mucho más la forma de una espeificación.

Programar es transformar datos, y el operador `|>` hace la transformación explícita.

> TODO: Fizz Buzz challenge

# Directivas para módulos

En la declaración de módulos de Elixir se proveen de tres directivas y una macro que facilitan el re-uso de código.

Mantén en mente que son llamadas directivas por que tienen un *alcance léxico*, mientras que la macro es un punto de extensión común que permite usar un módulo para inyectar código.

### alias

Permite referir nombres de módulos de forma corta o abreviada.

```elixir
defmodule Stats do
  alias Math.list, as: list
end
```

La llamada de ´alias´ sin la opción `as:` sería similar, usamos ´as:´ para renombrar el usp dentro del módulo. Es decir `alias Math.List` es lo mismo que `alias Math.List, as: List`.

Ahora bien, nota que tiene un alcance léxico, lo cual permite establecer aliases dentro de functiones específicas.

```elixir
defmodule Math do
  def plus(a, b) do
    alias Math.List
    # ...
  end

  def minus(a, b) do
    # ...
  end
end
```

En este caso la invocación será sólo válida dentro de la función `plus`.

> NOTA: Todos los módulos definidos en Elixir están definidos dentro de sus mismo espcaio de nombres, Sin embhargo, por conveniencia, podemos omitir "Elixir.", cuando los referenciamos.

```elixir
defmodule Example do
  def compile_and_go(source) do
    alias My.Other.Module.Parser, as: Parser
    alias My.Other.Module.Runner, as: Runner
    source
    |> Parser.parse()
    |> Runner.execute()
  end
end
```

### import

La directiva `import` brinda las funciones y/o macros de un módulo dentro del alcance actual. Si usas demasiado un módulo en particular, entonces `import` puede reducir el uso de la referencia eliminando la necesidad de nombrarlo varias veces.

```elixir
defmodule Example do
  def func1 do
    List.flatten [1,[2,3],4]
  end
  def func2 do
    import List, only: [flatten: 1]
    flatten [5,[6,7],8]
  end
end
```

La forma general de `import` es:

```elixir
import Module [, only: | except: ]
```

El segundo parámetro opcional permite controlar cuáles funciones o macros son importadas. Las cuáles deben ser escritas como una lista de _nombre: aridad_. Esto debido a que es buena idea usar las funciones necesarias.

```elixir
import List, only: [ flatten: 1, duplicate: 2 ]
```

### require

La directiva `require` le indica al compilador la carga de un módulo específico antes de compilar el contenido del mismo. Esto es sólo necesario si quieres referenciar macros de un módulo específico

```shell
iex(3)> Integer.is_odd(3)
** (CompileError) iex:3: you must require Integer before invoking the macro Integer.is_odd/1
    (elixir 1.10.0) src/elixir_dispatch.erl:107: :elixir_dispatch.dispatch_require/6
iex(3)> require Integer
Integer
iex(4)> Integer.is_odd(3)
true
```

Aunque `require/2` no es usado frecuentemente, es bastante importante. Haciendo `require` de un módulo asegura que está compilado y cargado. Esto es muy útil cuando necesitamos acceso a las macros de un módulo:

De esta forma, `require` e `import` tienen un propósito superpuesto. Ambas pueden ser usadas para acceder macros en otros módulos, aunque sus efectos en el espacio de nombres difieren.

### use

La macro `use` es frecuentemente usada como un punto de extensión. Esto significa que le vamos a permitir a un módulo inyectar cualquier código en el módulo actual, cómo si se importará a sí mismo u otros módulos, definir nuevas funciones, establecer el estado del módulo, etc.

```elixir
defmodule AssertionTest do
  use ExUnit.Case, async: true

  test "always pass" do
    assert true
  end
end
```

Detrás de esto, la macro `use` requiere el módulo indicado e invoca la retrollamada `__using__/1` sobre el módulo para inyectar algo de código en el contexto actual. Algunos componentes y módulos a desarrollar usarán este mecanismo para poblar el módulo con algún comportamiento básico, el cuál tiene la intención de ser completado o sobreescrito.

Describiendo lo que ocurre con `use`, sería algo como lo siguiente:

```elixir
defmodule Example do
  use Feature, option: :value
end
```

Es compilado en:

```elixir
defmodule Example do
  require Feature
  Feature.__using__(option: :value)
end
```

#### Entendimiento de las macros

Pongamos como ejemplo la creación de una macro:

```elixir
defmodule Hello do
  defmacro __using__(_opts) do
    quote do
      def hello(name), do: "Hi, #{name}"
    end
  end
end
```

Ahora usemos la macro dentro de un módulo de ejemplo:

```elixir
defmodule Example do
  use Hello
end
```

Y al ejecutar el código del módulo `Example` podemos ver cómo la función `hello/1` puede ser usada también:

```bash
iex> Example.hello("MakingDevs")
"Hi, MakingDevs"
```

**¿Qué sucedió?**
Con ayuda de `use` se invocó el callback `__using__/1` del módulo `Hello`, el cuál agregó el código al módulo respectivamente, con lo cual podemos invocar a la función del módulo que declara la macro. Ahora hagamos una modificación:

```elixir
defmodule Hello do
  defmacro __using__(opts) do
    greeting = Keyword.get(opts, :greeting, "Hi") # Usamos el parámetro

    quote do
      def hello(name), do: unquote(greeting) <> ", " <> name
    end
  end
end
```

Y ahora apliquemos la macro en el módulo con una variante:

```elixir
defmodule Example do
  use Hello, greeting: "Hola"
end
```

Si lo probamos nuevamente deberíamos de ver algo cómo lo siguiente:

```shell
iex> Example.hello("MakingDevs")
"Hola, MakingDevs"
```

Aún no profundizaremos en la creación de macros, sin embargo, es útil conocer el funcionamiento para poder diferenciarla y usarla apropiadamente; algunos frameworks y bibliotecas basan su uso en las macros por la gran flexibilidad y poder de implementarlas.

Por ejemplo *Ecto* nos ayuda a simplificar su uso a través de sus macros:

```elixir
defmodule Ecto.Schema do
  # ...
  defmacro __using__(_) do
    quote do
      import Ecto.Schema, only: [schema: 2, embedded_schema: 1]

      @primary_key nil
      @timestamps_opts []
      @foreign_key_type :id
      @schema_prefix nil
      @field_source_mapper fn x -> x end

      Module.register_attribute(__MODULE__, :ecto_primary_keys, accumulate: true)
      Module.register_attribute(__MODULE__, :ecto_fields, accumulate: true)
      Module.register_attribute(__MODULE__, :ecto_query_fields, accumulate: true)
      Module.register_attribute(__MODULE__, :ecto_field_sources, accumulate: true)
      Module.register_attribute(__MODULE__, :ecto_assocs, accumulate: true)
      Module.register_attribute(__MODULE__, :ecto_embeds, accumulate: true)
      Module.register_attribute(__MODULE__, :ecto_raw, accumulate: true)
      Module.register_attribute(__MODULE__, :ecto_autogenerate, accumulate: true)
      Module.register_attribute(__MODULE__, :ecto_autoupdate, accumulate: true)
      Module.put_attribute(__MODULE__, :ecto_autogenerate_id, nil)
    end
  end
  # ...
end
```

Mas información en: https://github.com/elixir-ecto/ecto/blob/master/lib/ecto/schema.ex

### Atributos de módulo

Cada módulo de Elixir tiene asociados algunos metadatos. Cada elemento es llamado un _atributo_ de módulo y es identificado por su nombre. Tiene dos propósitos: pueden ser usadas como constantes en tiempo de compilación, y pueden ser registrados para usarse en tiempo de ejecución. Por ejemplo: @moduledoc, @typedoc, @doc, y los que sean declarados por el programador.

```elixir
defmodule Circle do
  @moduledoc "Implements basic circle functions"
  @pi 3.14159
  @doc "Computes the area of a circle"
  def area(r), do: r*r*@pi
  @doc "Computes the circumference of a circle"
  def circumference(r), do: 2*r*@pi
end
```

Prueba en la _iex_ obtener la ayuda de éste módulo y sus funciones.

> **TODO: Revisar los Doctest de Elixir y la generación de la documentación basada en metadatos con [ex_doc](https://github.com/elixir-lang/ex_doc)**

#### Documentando funciones

Aquí un ejemplo de cómo documentar los módulos y las funciones:

```elixir
defmodule KeyboardHeroes.Logic.Game do
  @moduledoc """
  This module handle the logic of TypeRacer Game
  """

  @enforce_keys [:paragraph]
  defstruct players: [], paragraph: nil, letters: [], uuid: nil, positions: [], timer: nil, status: nil

  alias __MODULE__
  alias KeyboardHeroes.Logic.Player

  @doc """
  Creates a new game with a paragrapah to play and type.
  This game starts with zero players.
  """
  def new(paragraph) do
    %Game{
      paragraph: paragraph,
      letters: String.codepoints(paragraph),
      uuid: :rand.uniform(10_000),
      timer: KeyboardHeroes.TimerServer.start_link, status: "waiting"
    }
  end

  @doc """
  Adds a new player to a game.
  TODO: Validates if the user already exists
  """
  def add_player(game, username) do
    new_player = %Player{username: username}
    %{game | players: [new_player | game.players]}
  end

  # ...

end
```

### Typespecs ???
### Llamadas a Erlang ???

## Recursion

Es una de las característica apropiadas para el manejo de listas, iteraciones y ejecución del código contenido en procesos. La forma en cómo se programa usando la recursión en la mayoría de los casos puede causar confusión al principio pero una vez entendido tendrá todo el caso orientar las soluciones a la recursividad.

En la muchos casos, el uso de recursión se verá acompañado de coincidencia de patrones y guardas para su refinamiento.

```elixir
defmodule NaturalNumbers do
  def print(1), do: IO.puts(1)
  def print(n) do
    print(n - 1)
    IO.puts(n)
  end
end
```

Si usamos algún número negativo o incluso el cero puede que ocurra alguna excepción; puedes corregir esto con alguna guarda.

```elixir
defmodule MyList do
  def length([]), do: 0
  def length([_head|tail]), do: 1 + length(tail)
end
```

Observa con cuidado los elementos de coincidencia de patrones, de-estructuración y recursión.

> TODO: Realiza una función que sume los números de una lista.

O bien, un ejemplo clásico puede ser cómo el siguiente:

```elixir
defmodule Factorial do
  def of(1), do: 1
  def of(n), do: n * of(n-1)
end
```

> **TODO:** Implementa la secuencia de Fibonacci y el Máximo Común Divisor

## Tail Call Optimization

Lo anterior se conoce como un proceso de recursión linear, y se podría desarrollar de la siguiente manera:

<img src="https://mitpress.mit.edu/sites/default/files/sicp/full-text/book/ch1-Z-G-7.gif" alt="alt linear_recursion" style="background-color: white; padding: 10px;">

Lo que queremos es evitar el apilamiento que provoque el desbordamiento de la pila de ejecución, para ello lo que hacemos es una optimización tal como la siguiente:

<img src="https://mitpress.mit.edu/sites/default/files/sicp/full-text/book/ch1-Z-G-10.gif" alt="alt linear_recursion" style="background-color: white; padding: 10px;">

El ejemplo anterior podría quedar de la siguiente manera:

```elixir
defmodule Factorial do
  def of(n), do: compute(n, 1)
  defp compute(1, accum), do: accum
  defp compute(n, accum), do: compute(n-1, accum * n)
end
```

Este son el tipo de optimizaciones que debes de considerar cuándo programas en Elixir, el uso de recursividad con optimización de cola permitirá que no se sobrecargue la pila de ejecución y además podrás hacer uso natural de la estructura de los procesos y su estado.

## Higher Order Functions

En breve, son funciones que pueden tomar funciones como parámetros y regresan funciones como valores. No es algo específico de Elixir, se puede hacer en cualquier otro lenguaje, sin embargo, Elixir las trata de forma nativa como parte natural del lenguaje.

Es decir, cumplen con tres características:

1. Una función podría pasarse como argumento de otra función.
2. Una función podría ser asignada a una variable.
3. Una función podría regresar otra función cómo su resultado al ser invocada.

## Flujo de control

El flujo de ejecución lo determinamos con las llamadas de las funciones, de hecho es recomendable hacerlo de esta manera, así podrás ampliar funcionalidad de una forma simple, sin embargo, existe una manera alternativa de determinar por donde irán los datos en la transfromación.

Ahora bien, retomemos un poco de los conceptos vistos previamente y complementemos con su entendimiento más profundo.

- Paréntesis opcionales
- Keywords list

Y entendamos un par de conceptos más:

### Keyword list como último argumento

Si el último argumento en la llamada a una función es un _keyword list_ entonces los corchetes cuadrados `[]` pueden ser omitidos. Es decir:

```elixir
if(condition, [{:do, this}, {:else, that}])
if(condition, [do: this, else: that])
if(condition, do: this, else: that)
```

### Los bloques `do` / `end`

Una última sintaxis conveniente son los bloques `do`/ `end`. Los cuales son equivalentes a las _keyword list_ como el último argumento de la llamada a una función donde el contenido del bloque está rodeado por paréntesis. Por ejemplo:

```elixir
if true do
  this
else
  that
end
```

Es lo mismo cómo si escribimos lo siguiente:

```elixir
if(true, do: (this), else: (that))
```

Los paréntesis son importantes para soportar múltiples expresiones:

```elixir
if true do
  this
  that
end
```

Es lo mismo que escribir:

```elixir
if(true, do: (
  this
  that
))
```

Dentro de los bloques `do`/ `end` puedes introducir otras palabras clave, como `else` usado en el `if` anterior. Las palabras clave soportadas entre los bloques `do`/ `end` son estáticas y son:

- `after`
- `catch`
- `else`
- `rescue`

Veremos como funcionan en construcciones como `receive`, `try`, y otras.

#### Notas de AST

```elixir
defmodule Math do
  def add(a, b) do
    a + b
  end
end
```

Puede ser escrito de la forma:

```elixir
defmodule(Math, [
  {:do, def(add(a, b), [{:do, a + b}])}
])
```

### `case`

Nos permite comparar un valor conta varios **patrones** hasta que encontremos una que coincida.

```elixir
case valor_a_evaluar do
  coincidencia_1 -> # código a ejecutar
  coincidencia_2 -> # código a ejecutar
  coincidencia_3 -> # código a ejecutar
  # ...
  _ -> # código a ejecutar si ninguno coincide
end

# coincidencia = patrón + [guarda]
```

Ejemplos:

```elixir
case File.open("some.txt") do
  {:ok, file} -> "It's open!"
  {:error, _reason} -> "Cannot open..."
end

pie = 3.14
case "cherry pie" do
  ^pie -> "Not so tasty"
  pie -> "I bet #{pie} is tasty"
end

case {1, 2, 3} do
  {1, x, 3} when x > 0 ->
    "Will match"
  _ ->
    "Won't match"
end

juan = %{name: "Juan", age: 27}
case dave do
  person = %{age: age} when is_number(age) and age >= 21 ->
    IO.puts "You are cleared to enter the Foo Bar, #{person.name}"
  _ ->
    IO.puts "Sorry, no admission"
end

defmodule Drop do
  def fall_velocity(planemo, distance) when distance >= 0 do
    gravity = case planemo do
      :earth -> 9.8
      :moon -> 1.6
      :mars -> 3.71
    end
    :math.sqrt(2 * gravity * distance)
  end
end
```

Recuerda la definición de las funciones y su aridad, verás que hay una relación cercana.

**Nota:** La evaluación hecha por `case` puede ser asignada a alguna variable, o ser el último bloque de código


### `cond`

Es muy similar a la sentencia `case` pero sin coincidencia de patrones. Ejecuta el primer bloque de código que corresponde a una condición verdadera. Considera siempre poner una condición que evalue a verdadero, por ejemplo: `true`.

```elixir
cond valor_a_evaluar do
  condicion_1 -> # bloque a ejecutar
  condicion_2 -> # bloque a ejecutar
  # ...
  condicion_n -> # bloque a ejecutar
end
```

Ejemplos:

```elixir
test_fun = fn(x) ->
  cond do
    x > 0 ->
      :positive
    x < 0  ->
      :negative
    true ->
      :zero
  end
end

cond do
  name == nil or name == "" ->
    "?"
  String.contains?(name, " ") ->
    split_name = name |> String.split(" ")

    first_letter = split_name |> List.first() |> String.slice(0, 1)
    last_letter = split_name |> List.last() |> String.slice(0, 1)

    "#{first_letter}#{last_letter}"
  true ->
    name |> String.slice(0, 1)
end

cond do
  Map.has_key?(params, "absence_id") ->
    %{register | absence_id: params["absence_id"]}
  Map.has_key?(params, "substitution_id") ->
    %{register | substitution_id: params["substitution_id"]}
  Map.has_key?(params, "delayment_id") ->
    %{register | delayment_id: params["delayment_id"]}
  true ->
    register
end
```

**Nota:** La evaluación hecha por `cond` puede ser asignada a alguna variable, o ser el último bloque de código

### `with`

Permite construir una serie de comprobaciones, una enseguida de otra hasta concluir el bloque de ejecución. Si alguna no cumple la condición entonces el código entra en el bloque de errores y busca tal error.

```elixir
with
  coincidencia_1 <- funcion_o_variable_1,
  coincidencia_2 <- funcion_o_variable_2,
  coincidencia_N <- funcion_o_variable_N
do
  # bloque_de_codigo
else
  coincidencia_error_1 -> bloque_error_1
  coincidencia_error_2 -> bloque_error_2
  coincidencia_error_N -> bloque_error_N
end
```

Generalmente, ayuda mucho a simplificar el uso anidado de sentencias `case`, lo cual permite reducir complejidad de lectura.

Por ejemplo:

```elixir
file_data = %{name: "noname.txt"}
case Map.fetch(file_data, :name) do
  {:ok, name} ->
    case File.read(name) do
      {:ok, contents} ->
        contents
        |> String.split("\n", trim: true)
        |> Enum.map(&String.reverse/1)
        |> Enum.join("\n")
      {:error, :enoent} ->
        IO.puts "Could not find a file called #{name}"
    end
  :error -> "No key called :name in file_data map"
end

# Puede ser re-escrito de la siguiente forma

file_data = %{name: "noname.txt"}
case Map.fetch(file_data, :name) do
with {:ok, name} <- Map.fetch(file_data, :name),
     {:ok, contents} <- File.read(name) do
  contents
  |> String.split("\n", trim: true)
  |> Enum.map(&String.reverse/1)
  |> Enum.join("\n")
  |> IO.puts
else
  :error -> ":name key missing in file_data"
  {:error, :enoent} -> "Couldn't read file"
end
```

Un caso de uso común es con el uso de Ecto:

```elixir
case Repo.insert(changeset) do
  {:ok, user} ->
    case Guardian.encode_and_sign(user, :token, claims) do
      {:ok, token, full_claims} ->
        important_stuff(token, full_claims)
      error ->
        error
    end
  error ->
    error
end

# Puede ser re-escrito

with {:ok, user} <- Repo.insert(changeset),
     {:ok, token, full_claims} <- Guardian.encode_and_sign(user, :token, claims) do
  important_stuff(token, full_claims)
end
```

### Exceptions

Es importante saber que las excepciones en Elixir NO son estructuras de control de flujo. Las excepciones están diseñadas para cosas que **nunca deberían** pasar en una operación normal. Significa que cuando la base de datos cae, o falla abrir un archivo de configuración, podría considerarse excepcional. Sin embargo, abrir un archivo que un usuario registró no es excepcional.

Alcanzar una excepción con la función `raise`, y de la forma más simple con un string genera una excepción del tipo `RuntimeError`.

```shell
iex(1)> raise "You're doing wrong!!!"
** (RuntimeError) You're doing wrong!!!
```

Puedes pasar además el tipo de excepción, junto con otros atributos.

```shell
iex(1)> raise RuntimeError
** (RuntimeError) runtime error

iex(1)> raise RuntimeError, message: "Custom error message"
** (RuntimeError) Custom error message
```

El uso de excepciones es mucho menor en Elixir más que en otros lenguajes, la filosofía de diseño es que los errores deberían ser propagados a un proceso externo y supervisado.

Más detalle en la [documentación oficial](https://hexdocs.pm/elixir/Kernel.html#raise/1).

> Date una vuelta por la [documentación de Elixir](https://hexdocs.pm/elixir/Kernel.html) para entender los principios sobre los que se rige el lenguaje.
