# Principios de programación funcional

En definitiva entender los conceptos ayuda demasiado a la comprensión de las aplicaciones.

Quiero explicar aquí la esencia de los principios de la programación funcional, los cuáles los he tomado del lenguaje LISP, y que aplican perfectamente a cualquier otro lenguaje de programación que se nombre a sí mismo como funcional.

> ### La taza vacía
> Cuenta una vieja leyenda que un famoso samurái fue de visita a la casa de un maestro zen. Al llegar se presentó ante él. Durante un buen rato el guerrero le contó todos los títulos y aprendizajes que había obtenido durante años de grandes sacrificios y largos estudios.
>
> Después de tan formal presentación, le explicó que había ido a verlo para que le enseñara los secretos del conocimiento zen.
>
> El maestro le miró y, por toda respuesta, se limitó a invitarle a sentarse y ofrecerle una taza de té.
>
> El maestro zen comenzó a ponerle el té en la taza al samurái. Aparentemente distraído, sin dar muestras de mayor preocupación, el maestro vertió el té en la taza del guerrero, y continuó vertiendo té aún después de que la taza estuviese llena.
>
> Consternado, el samurái advirtió al maestro que la taza ya estaba llena, y que el té se escurría por la mesa.
>
> El maestro le respondió con tranquilidad:
>
> Exactamente señor. Usted ya viene con la taza llena, ¿Cómo podría usted aprender algo?
>
> Ante la expresión incrédula del guerrero, el maestro enfatizó:
>
> *A menos que su taza esté vacía, no podrá aprender nada.*
>
> Desde ese día, el samurái se convirtió en un humilde discípulo del maestro, y con los años llegó a ser un famoso maestro zen.

---

## Principio Funcional

La programación funcional es un acercamiento matemático que tuvo como pioneros a los creadores de LISP. La programación funcional pone ciertas restricciones sobre el programador, pero puede resultar en código muy elegante. Cuándo se usa cada variable que es declarada por una función debe ser una de las siguientes:

- Un parámetro que pasó a la función
- Una variable local creada dentro de la función
- Una constante

Además la programación funcional no permite que una función tenga *efectos colaterales*, esto significa que una función no pueda escribir a disco, imprimir mensajes a la pantalla, o hacer cualquier cosa más que regresar un resultado. El objetivo es escribir mayormente un programa usando "código funcional", mientras se retiene un pequeño fragmento de código que hace cualquier cosa sucia, a cosas no funcionales que todavía son necesarias.

### Ventajas

Escribir código en un estilo funcional garantiza que una función hace sólo una cosa(regresa un valor) y depende de sólo una cosa(los parámetros que le son pasados). Esto lo hace muy fácil de depurar. No importa cuántas veces corras una función, tantas veces que pases por ella los mismos datos, siempre obtendrás el mismo resultado.

### Profundizando de forma práctica

Observa muy bien dónde está el _trabajo sucio_:

```elixir
defmodule Functional do
  def unique_letters(name) do
    "Hello " <> List.unique(name)
  end
  def ask_and_respond  do
    IO.puts "What's your name?"
    IO.puts unique_letters(IO.gets())
  end
end
```

### Debilidades

La principal desventaja de la programación funcional es que algunos efectos laterales son casi siempre necesarios para que un programa actualmente haga algo. Esto significa que no puedes escribir un programa usable que tenga su código entero en el estilo funcional. Por lo menos una pequeña parte del código será no funcional.

---

## Principio de Macro

Las macros son una característica sorprendente. De hecho, la estructura del lenguaje Elixir permite habilitar el sistema de macros por un elemento denominado Homoiconocidad.

Dichas macros permiten agregar nueva funcionalidad a Elixir de una forma muy fundamental. Programadores experimentados pueden usar las macros para hacer que el compilador/interprete haga su trabajo de forma limpia y elegante.

### Ventajas

Usando macros, un programador experimentado puede minimizar la duplicación de código y adaptar el lenguaje al problema subyancente en cuestión. Esto conlleva a código más limpio y menos _bugs_.

### Profundizando de forma práctica

Las macros son tan poderosas que actualmente puedes escribir tu propia sintaxis para tus programas, como un `if/else` o `unless`.

```elixir
defmodule OurMacro do
  defmacro unless(expr, do: block) do
    quote do
      if !unquote(expr), do: unquote(block)
    end
  end
end

OurMacro.unless true, do: "Hi"
```

Las macros son muy usadas en los frameworks del ecosistema Elixir:

```elixir
defmodule HelloWeb.Router do
  use HelloWeb, :router # Macro

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", HelloWeb do
    pipe_through :browser

    get "/", PageController, :index
  end

end
```

[Consulta la documentación de Elixir para la creación de Macros.](https://elixir-lang.org/getting-started/meta/macros.html)

### Debilidades

Dado que las macros son muy poderosas, existe siempre el peligro de que los programadores abusen de ellas. El sobre uso puede hacer díficil que otros programadores entiendan el código.

---

## Principio de Reinicio

El apropiado manejo de excepciones es extremadamente difícil. Hay realmente sólo dos buenos acercamientos:

- No manejar excepciones del todo y sólo dejar que el programa muera cuando alguna ocurra.
- Manejar cada una de las excepciones en la forma más directa y específica posible.

Pero, ¿es verdaderamente posible manejar cada excepción potencial en tu código?

En cualquier programa que desarrolles siempre estarás propenso a errores de reserva de memoria, o desbordamiento de pila, o incluso errores que no consideraste por condiciones incompletas en la lógica de negocio y casos que estaban fuera del contexto de algún requerimiento. Esto hace imposible direccionar el uso tradicional de manejo de excepciones.

Incluso si una función de bajo nivel en la llamada de la pila atrapa y resuelve el origen de la excepción, el programa aún enfrenta un problema sin resolver: algunos de los datos han sido modificados, y algunos no, en un lenguaje de programación funcional se consideran estos mediante _reinicios_.

En un lenguaje que soporta reinicios, la función que ejecuta la operación de error puede notificar a otro componente, indicando que ha fallado. Otra función fuera de ella puede ahora manejar el error, y asegurarse del estado no se corrompió.

![alt supervisor](https://learnyousomeerlang.com/static/img/sup-tree.png)

### Ventajas

Usando reinicios, un bug(error) puede ser arreglado en un programa corriendo, lo que permite ejecutar "scripts en caliente" en aplicaciones de larga ejecución con solo una interacción insignificante.

### Profundizando de forma práctica

Todo comienza con el monitoreo de procesos:

```elixir
# Crea un nuevo proceso
pid = spawn(fn ->
  :timer.sleep 500
  raise("Sorry, my friend.")
end)

# Establece un monitor para el proceso
ref = Process.monitor(pid)

# Espera por un mensaje de baja para el proceso dado
receive do
  {:DOWN, ^ref, :process, ^pid, :normal} ->
    IO.puts "Normal exit from #{inspect pid}"
  {:DOWN, ^ref, :process, ^pid, msg} ->
    IO.puts "Received :DOWN from #{inspect pid}"
    IO.inspect msg
end
```

Sin embargo, de forma practica estaremos usando un componente de nombre `Supervisor`.

```elixir
defmodule MD.Supervisor do
  use Supervisor

  def start_link(opts) do
    Supervisor.start_link(__MODULE__, :ok, opts)
  end

  @impl true
  def init(:ok) do
    children = [
      MD.Worker
    ]

    Supervisor.init(children, strategy: :one_for_one)
  end
end
```

### Debilidades

Aunque Erlang/Elixir tienen uno de los meecanismos más avanzados en el manejo de excepciones, es todavía difícil manejar cada excepción apropiadamente en el código. Sin embargo, el **reinicio** ofrece una habilidad única para arreglar un programa corriendo y le permite continuar operando, lo cuál no es usualmente posible en otros lenguajes.

---

## Principio de Establecimiento de valor

Hemos visto el atado de variables, en dónde, una vez que se usa el operador `=` se le realiza un vinculación de lo que está del lado izquierdo con lo que está del lado derecho, sin embargo, esta variable no puede ser modificada a menos que se vuelva a re-víncular. Lo interesante son las maneras en cómo podemos trabajar con algunos de estos casos en las diferentes estructuras de datos.

### Ventajas

Cuando tienes estructuras de datos vínculadas y complicadas, es a menudo fácil de entender el código que entrega datros de una ubicación específica, en lugar de entender el código que establece un valor en la misma ubicación. Si quieres establecer un valor en un punto específico es una estructura complicada, realmente necesitas trabajar hacia atrás a través de la estructura para descubrir cómo puedes cambiarla. Tener código simple es una gran manera de evitar _bugs_.

### Profundizando de forma práctica

Un caso que demuestra de forma práctica este principio es cuando intentamos modificar los valores contenido en un mapa, por ejemplo:

```elixir
structure = %{ list: [] }
IO.inspect(structure.list)
# structure.list = [1] # (CompileError)
%{structure  | list: [1,2,3,4]}
structure # %{list: []}
structure = %{structure  | list: [1,2,3,4]}
structure # %{list: [1, 2, 3, 4]}
```

Lo mismo pasaría para cualquier otra estructura de datos que tengamos en mente, ya sea simple como una lista o más elaborada como por ejemplo el uso de `defstruct`, y el anidamiento de otros tipos complejos.

Para comprender mejor la forma en cómo están diseñadas las estructuras basadas en módulos es recomendable revisar [la documentación de `defstruct`](https://hexdocs.pm/elixir/Kernel.html#defstruct/1).

### Debilidades

Cuando haces una re-asignación o re-vínculación de una estructura de datos existente puedes causar un efecto colateral, el cuál viola uno de los principios de la programación funcional. Eso siginifica que no pueden ser usados cuando se usa un estilo de programación puramente funcional.

---

## Principio de DSL

Debido a que Elixir tiene una sintaxis muy simple(basada en funciones, susus llamadas y keyword lists), es fácil usarlo para construir tu propio lenguaje de programación, diseñado para un dominio específico. Como los DSL tienden a hacer uso extensivo y pesado del sistema de macros de Elixir, representan una forma extrema de programación de macros, transformando Elixir en un lenguaje nuevo de programación.

Sin embargo, puedes empezar con cosas muy simples sin la necesidad del uso de macros, cada estructura de datos y cada función definida en el módulo es parte de un propio DSL.

### Ventajas

Escribir un nuevo lenguaje de programación!!!

### Profundizando de forma práctica

Hay casos dónde usar macros y módulos para construir un DSL es útil, y cuándo estás creando alguno deberías de considerar algo cómo lo siguiente:

> data > functions > macros

Por ejemplo:

```elixir
# 1. data
import Validator
validate user, name: [length: 1..100],
               email: [matches: ~r/@/]

# 2. functions
import Validator
user
|> validate_length(:name, 1..100)
|> validate_matches(:email, ~r/@/)

# 3. macros
defmodule MyValidator do
  use Validator
  validate_length :name, 1..100
  validate_matches :email, ~r/@/
end

MyValidator.validate(user)
```

Otros ejemplos de DSL's ya existentes son:

```elixir
temple do
  h2 "todos"

  ul class: "list" do
    for item <- @items do
      li class: "item" do
        div class: "checkbox" do
          div class: "bullet hidden"
        end

        div item
      end
    end
  end
end
```

De la biblioteca [temple](https://github.com/mhanberg/temple).

O directo desde el módulo `Ecto.Query`, una forma hacer consultas es:

```elixir
import Ecto.Query, only: [from: 2]

query = from u in "users",
          where: u.age > 18,
          select: u.name

Repo.all(query)
```

Es decir, deberías de considerar si un DSL se puede crear con tan sólo la definición de una estructura de datos, como un simple `keyword list` entonces es suficiente; lo siguiente a considerar son un grupo de funciones que hagan el trabajo, o bien una transformación mucho más compleja si quieres usar macros que modifiquen la estructura de tu módulo y agreguen funciones declaradas.

### Debilidades

Los DSL son lenguajes de programación que puedes crear por tu cuenta, con ello puedes definitivamente dispararte en el pie si no eres cuidadoso, especialmente si usas macros, son embargo si conservas los primeros dos niveles de DSL con estructuras y funciones te puedes mantener a salvo. Es muy fácil crear código en un lenguaje que es imposible entender para otros(incluso para ti).

---

## Principio de Meta-objeto

¿Qué es la POO? Alan Kay dice lo siguiente:

> OOP to me means only messaging, local retention and protection and hiding of state-process, and extreme late-binding of all things. It can be done in Smalltalk and in LISP. There are possibly other systems in which this is possible, but I'm not aware of them.

Fuente: [Dr. Alan Kay on the Meaning of "Object-Oriented Programming"](http://userpage.fu-berlin.de/~ram/pub/pub_jf47ht81Ht/doc_kay_oop_en)

### Ventajas

Puedes usar la forma de diseño de OO para determinar la estructura y relación de ciertos tipos de datos; el pensamiento de relación entre objetos ayudará a determinar una jerarquía de estructuras y lo que debe de contener cada una.

### Profundizando de forma práctica

```elixir
send self(), {:hello, "world"}

receive do
  {:hello, msg} -> msg
  {:world, _msg} -> "won't match"
end
```

Sin embargo, estamos hablando de Meta-objetos, es decir, elementos que simulan o podrían simular ser objetos, los proceso por sí mismos podrían serlo y contener internamente una estructura de datos que conserve su estado actual, nos apoyaremos de listas y mapas para crear tal estado.o

Ahora bien, también podemos usar la OO para diseñar de forma entendible la manera en cómo se deben de transformar los datos, definir la estructura y crear algunas restricciones, por ejemplo:

```elixir
defmodule Person do
  defstruct name: "", age: 0, stage: :baby

  def new() do
    %Person{}
  end

  def increment_age(person) do
    %Person{person | age: person.age + 1}
  end

  def babify(person) do
    %Person{person | age: 1, stage: :baby}
  end

  def can_retire?(person, retirement_age) do
    person.age >= retirement_age
  end
end
```

### Desventajas

Seguramente estarás pensando en resolver problemas con el estilo de POO. Tendrás que vaciar esta taza!!!

---

## Principio de Continuación

No disponible en Elixir, sin embargo, existe la posibilidad de permitir hacer "viajes en el tiempo" dentro de tu código. Esto permite hacer que los programas corran hacia atrás, hacia los lados o de otras formas muy locas. Es útil para implementar ténicas de programación avanzadas denominadas _programación no determinista_, en la cual escribes código que ofrece a la computadora múltiples opciones de que hacer después. Si una elección no es satifactoria, la computadora puede "regresar el tiempo" con continuación para intentar un camino distinto.

---

## Principio de Brevedad

Elixir te permite escribir código que es increíblemente conciso pero no se ve cómo si tu gato hubiera caminado por tu teclado. Esto es posible por varias características que hemos mencionado como macros, programación funcional y el sistema dinámico de tipos.

### Ventajas

Con esta característica el objetivo es escribir programas que sean cortos. Esta diseñada para permitirte decir lo que quieras decir en la manera más concisa posible, sin dejar espacio a errores por ocultar alguna función o transformación que suceda en el paso de los datos.

### Profundizando de forma práctica

Podemos ver algo de esto en el uso de Phoenix, específicamente de Plug:

```elixir
get "/", do: send_resp(conn, 200, "Welcome to #{conn.assigns.name}")
get "/active", do: send_resp(conn, 200, "5 Active Jobs")
get "/pending", do: send_resp(conn, 200, "3 Pending Jobs")
match _, do: send_resp(conn, 404, "Not found")
```

Una vez que puedes entender el uso de los principios de la PF, puedes empezar creando cosas como las siguientes:

```elixir
@doc"""
  This function get the periods from endpoint and
  transform to periods with dates in spanish,
  filter removing DELETED periods,
  and sort by month.
"""
def filter_periods( periods_from_store ) do
  periods_from_store
    |> get_periods_in_spanish
    |> get_periods_for_show
    |> sort_by_month
end

@doc"""
  This function parsed the calendar list from broker,
  so it parses the period as camelCase to our Phoenix app
"""
def get_periods_in_spanish( periods ) when is_list( periods ) do
  for period <- periods do
    parse_calendar( period["captureDate"], period )
  end
end
```

### Desventajas

Encontrar un conjunto óptimo y conciso de comandos es difícil. Con tantos comandos disponibles, el código puede ser difícil de entender, debido a que es complejo recordar lo que cada función hace. Con muy pocos comandos, los programas pueden volverse demasiado voluminosos.

---

## Principio de Multi-núcleo

Ahora que las computadoras tienen múltiples núcleos, hay mucho interés en encontrar formas elegantes para escribir código multinúcleo/multihilo.

### Modelo de actores

Elixir hace uso de una aproximación sofisticada a la concurrencia, adoptada por Erlang y es el modelo de Actores de Carl Hewitt.

- En este modelo, los actores son entidades independientes que corren en paralelo.
- Los actores **encapsulan el estado que puede cambiar con el tiempo**
  - Pero dicho estado **no es compartido** con otro actor.
  - Cómo resultado, no hay condiciones de carrera.
- Los actores **se comunican enviando mensajes** entre ellos
  - Un actor procesará los mensajes de forma **secuencial**
- La concurrencia pasa por muchos actores corren en paralelo
  - pero cada actor por si mismo es un programa secuencial
    - una abstracción con la cual los programadores se sienten cómodos
- Los actores son llamados **procesos**
  - En la mayoría de los lenguajes y sistemas operativos los procesos son entidades **pesadas**
  - En Elixir, un proceso es **muy ligero** en términos de consumo de recursos y costos de inicio; más ligero incluso que los hilos.
- Los programas en Elixir quizá lancen **cientos de procesos todos corriendo concurrentemente**

### STM

Una aproximación popular es usar estructuras de datos funcionales junto con un sistema de memoria transaccional de software(STM).

Usando STM puedes compartir estructuras de datos complejas entre diferentes componentes, con garantía de que ninguno verá información incosistente en los datos, incluso si intenta leer datos compartidos mientras otro está tratando de leerlo.

La forma que tiene Elixir de implementarlo es a través de un componente de Erlang llamado [ETS](https://elixir-lang.org/getting-started/mix-otp/ets.html).

### Ventajas

El código multihilo tiende a ser muy **buggy**. Usando el modelo de actores y STM, puedes aumentar considerablemente tus probabilidades de escribir software multiproceso sin errores.

### Desventajas

Dependemos de los CPU's de los equipos, sin embargo, cada vez más dispositivos incrementan la cantidad de núcleos en sus arquitecturas, los cuáles, no son aprovechados por los lenguajes de programación comúnmente usados.

---

## Principio Lazy

Un lenguaje de programación funcional ejecutará cálculos solamente si el compilador determina que es absolutamente necesario producir un resultado viable. Elixir incluye programación _lazy_ como una caracterísitica dentro del módulo Stream. Sin embargo, tiene algunos otros elementos dónde también se puede apreciar la parte _perezosa_ del lenguaje.

### Ventajas

Los lenguajes _lazy_ te permiten crear estructuras de datos infinitamente grande(tanto cómo para que no tengas que usar todos los datos), lo cual permite tener más código que sea formulado como transformación de grandes estructuras de datos. En general, **es mucho más fácil depurar estructuras que depurar algoritmos**. Los algoritmos envuelven pasos que se desarrollan con el tiempo, y para entenderlos usualmente necesitas verlos conforme se están ejecutando. Los datos, por otro lado, existen independientes del tiempo, lo cuál significa que puedes encontrar errores en una estructura sólo viéndola.

### Profundizando de forma práctica

```elixir
stream = 1..3
|> Stream.map(&IO.inspect(&1))
|> Stream.map(&(&1 * 2))
|> Stream.map(&IO.inspect(&1))
Enum.to_list(stream)

1..100_000 |> Stream.map(&(&1 * 3)) |> Stream.filter(odd?) |> Enum.sum
```

### Desventajas

Desde que Elixir es _lazy_ escoge el orden en el cuál el código corre, puede provocar dolores de cabeza de depuración si intentas rastrear tu código mientras se ejecuta.
