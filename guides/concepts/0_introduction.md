# Introducción al lenguaje Elixir

> "Elixir es lenguaje de programación funcional construido sobre la máquina virtual de Erlang. Es un lenguaje dinámico con sintaxis flexible con soporte de macros que aprovecha las capacidades de Erlang para construir aplicaciones concurrentes, distribuidas y tolerantes a fallas con actualizaciones de código en vivo."

> "Elixir también proporciona soporte de primera clase para la coincidencia de patrones(pattern matching), el polimorfismo a través de protocolos (similares a los de Clojure), alias y estructuras de datos asociativas (generalmente conocidas como diccionarios o hashes en otros lenguajes de programación)."

> "Finalmente, Elixir y Erlang comparten el mismo bytecode y tipos de datos. Esto significa que puede invocar el código Erlang desde Elixir (y viceversa) sin ninguna conversión o impacto en el rendimiento. Esto permite a un desarrollador mezclar la expresividad de Elixir con la robustez y el rendimiento de Erlang."

## Piensa diferente...

- La orientación a objetos no es la única manera de diseñar código
- La programación funcional no necesita ser compleja o matemática
- Las bases de la programación no son asignaciones, sentencias `if`, y bucles.
- La concurrencia no necesita bloqueos, semáforos, monitores y cosas por el estilo(hilos).
- Los procesos no son recursos necesariamente caros.
- La metaprogramación no es exclusivo de un lenguaje.
- Incluso si es trabajo, la programación sigue siendo divertida.

## Transformación de datos

```elixir
def handle(request) do
  request
  |> parse
  |> rewrite_path
  |> log
  |> route
  |> track
  |> response
end
```

```elixir
defmodule Parallel do
  def pmap(collection, func) do
    collection
    |> Enum.map(&(Task.async(fn -> func.(&1) end)))
    |> Enum.map(&Task.await/1)
  end
end
```

## Acerca de Erlang

### Alta disponibilidad

- Tolerancia a fallos
- Escalabilidad
- Distribución
- Sensible
- Actualización en vivo

### Concurrencia en Erlang

Una de las razones principales para usa Erlang es la habilidad de manejar la concurrencia y la programación concurrente. Por concurrencia decimos que los programas pueden manejar varios hilos de ejecución al mismo tiempo. Cada procesador en el sistema está probablemente manejando un hilo o trabajo a la vez, pero intercambia entre trabajos a una taza que da la ilusión de correrlos al mismo tiempo. Es muy fácil crear hilos de ejecución en un programa basado en Erlang que permiten comunicarse unos con otros, los cuáles son llamados **procesos**.

**Importante**: El término proceso es usualmente usado cuando los hilos de ejecución no comparten datos entre ellos, y el término hilo se usa cuándo comparten datos entre ellos. Los hilos de ejecución en Erlang no comparten datos, por eso son llamados procesos.

## BEAM

![alt beam](https://3.bp.blogspot.com/-VJw-NpEbVyE/WMexqAlhxNI/AAAAAAAAFsA/qhpPIqfUKGIUIdpNGEqjIuki8inPGoA_gCLcB/s1600/erlang%2Bscheduler.png)

![alt distributed](https://blog.stenmans.org/theBeamBook/diag-d0423fbd28cb17bf33a7a8adc4b716db.png)

## OTP

- Patrones de concurrencia y distribución
- Detección de errores y recuperación en sistemas concurrentes
- Empaquetado de código dentro de las bibliotecas
- Despliegue de sistemas
- Actualizaciones de código en vivo

### ¿En qué lo podemos usar?

- Chat servers( Whatsapp, Ejabberd)
- Game servers(Wooga)
- Web frameworks(Phoenix)
- Bases de datos distribuidas(Riak, CouchDB)
- Servidores de ofertas en tiempo real
- Servicios de transmisión de video
- Demonios/servicios corriendo prolongadamente

Piensa en situaciones en dónde:

- Despachas a múltiples usuarios y clientes, a menudo entre miles y millones, y quieres mantener un nivel de respuesta decente.
- Permanecer arriba incluso en caso de falla, o tener mecanismos de conmutación elegantes por errores.
- Escalar con gracia, ya sea agregando más núcleos de CPU o máquinas.


## Acerca de Elixir

### Simplificación de código

```erlang
-module(sum_server).
-behaviour(gen_server).
-export([
  start/0, sum/3,
  init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2,
  code_change/3
]).

start() -> gen_server:start(?MODULE, [], []).
sum(Server, A, B) -> gen_server:call(Server, {sum, A, B}).

init(_) -> {ok, undefined}.
handle_call({sum, A, B}, _From, State) -> {reply, A + B, State}.
handle_cast(_Msg, State) -> {noreply, State}.
handle_info(_Info, State) -> {noreply, State}. terminate(_Reason, _State) -> ok.
code_change(_OldVsn, State, _Extra) -> {ok, State}.
```

```elixir
defmodule SumServer do
  use GenServer

  def start do
    GenServer.start(__MODULE__, nil)
  end
  def sum(server, a, b) do
    GenServer.call(server, {:sum, a, b})
  end
  def handle_call({:sum, a, b}, _from, state) do
    {:reply, a + b, state}
  end
end
```

### ¿Qué más tienes que saber?

- Velocidad
- Ecosistema
- Herramientas
- Tal vez deberías de considerar alguna opción si quieres hacer procesamiento de imagen, tareas de cómputo intensivas. Adicional si quieres hacer sistemas de "hard real-time" deberías de considerar otra cosa(Rust por ejemplo).

> ATENTO: Revisa la [API de referencia](https://hexdocs.pm/elixir/master/api-reference.html#content) de Elixir para encontrar el detalle de cada una de las cosas que veremos durante este entrenamiento.
