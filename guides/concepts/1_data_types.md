# Tipos de datos y operadores

## Palabras reservadas

Estas son las palabras reservadas en el lenguaje Elixir.

- `true`, `false`, `nil` - Usadas como átomos
- `when`, `and`, `or`, `not`, in - Usadas como operadores
- `fn` - Usada para la definición de funciones anónimas
- `do`, `end`, `catch`, `rescue`, `after`, `else` - Usadas para los bloques de código

## Tipos de valor
  - Enteros de tamaño arbitrario
    - Decimal (`1234`, `1_000_000`), hexadecimal (`0xcafe`), octal (`0o765`), binario (`0b1010`)
  - Números de punto flotante
    - `1.0`, `0.2456`, `0.314159e1`, `314159.0e-5`
  - Atomos
    - `:makingdevs` `:is_binary?` `:variable@2` `:<>` `:===` `:"func/3"` `:"long john silver"` `:эликсир` `:mötley_crüe`
  - Rangos
    - _start_.._end_
  - Expresiones regulares([PCRE](https://hexdocs.pm/elixir/Regex.html#summary))
- Tipos de sistema:
  - PIDs y ports
  - Referencias

## Tipos de colección
  - Tuplas
    - `{1, "uno", :uno, 1.0}` `{'a', "a"}` `{:ok, "Message"}`
  - Listas
    - `[5,6,7,8,9]` `[69,70,71]` `["a", 1, "4", :elem]`
  - Keyword Lists
    - `[name: "Juan", city: "CDMX", likes: "Programming"]`
    - `[{:name, "Juan"}, {:city, "CDMX"}, {:likes, "Programming"}]`
    - `DB.save record, [ {:use_transaction, true}, {:logging, "HIGH"} ]`
    - `DB.save record, use_transaction: true, logging: "HIGH"`
  - Mapas
    - `%{"organization" => "MakingDevs", "team_size" => 10, "skills" => ["ci","cd","xp","testing"]}`
    - `%{organization: "MakingDevs", team_size: 10, skills: ["ci","cd","xp","testing"]}`
  - Binarios
    - `bin = <<1,2,3,4>>`
    - `byte_size bin`
    - `bin = <<3 :: size(2), 5 :: size(4), 1 :: size(2)>>`

> Usa la función `i _tipo_` para obtener más información

## Módulos y Funciones
  ```elixir
  `sum = fn (a, b) -> a + b end`
  ```
  ```elixir
    fn
      parameter-list -> body
      parameter-list -> body ...
    end
  ```
  ```elixir
    defmodule MyModule do
      def my_function do
      end
    end
  ```

Veremos en profundidad las funciones más adelante.

_Nota: ¿Dónde están los strings y las estructuras?_

## Fechas y tiempos

Desde la versión de Elixir 1.3 se han agregado el módulo `Calendar` para el manejo específico de fechas y tiempo.

El tipo de dato `Date` mantiene el año, el mes, el día y una referencia a la regla de calendario.

```elixir
d1 = Date.new(2018, 12, 25)
{:ok, d1} = Date.new(2018, 12, 25)
d2 = ~D[2018-12-25]
d1 == d2
Date.day_of_week(d1)
Date.add(d1, 7)
inspect d1, structs: false
```

`~D` y `~T` son ejemplos de _sigils_, los cuales son una forma de construir valores literales, por ejemplo:

```elixir
d1 = ~D[2018-01-01]
d2 = ~D[2018-06-30]
```

El tipo `Time` contiene una hora, un minuto, un segundo y fracciones de un segundo. La fracción es almacenada en una tupla conteniendo los microsegundos y el número de dígitos significativos.

```elixir
{:ok, t1} = Time.new(12, 34, 56)
t2 = ~T[12:34:56.78]
t1 == t2
Time.add(t1, 3600) ~T[13:34:56.000000]
Time.add(t1, 3600, :millisecond) ~T[12:34:59.600000]
```

Hay dos tipos más para los tipos de fecha/tiempo: [`DateTime`](https://hexdocs.pm/elixir/DateTime.html) y [`NaiveDateTime`](https://hexdocs.pm/elixir/NaiveDateTime.html). La versión simplista que contiene sólo una fecha y un tiempo; `DateTime` agrega la habilidad de asociar una zona horaria. El sigil `~N[...]` construye el tipo `NaiveDateTime`.

## Strings y binarios

 Elixir tiene dos tipos de String: comilla simple y de doble comilla. Difieren significativamente en su representación interna, pero también tienen cosas en común.

- Strings soportan códificación UTF-8.
- Pueden contener secuencias de escape:
  - \a BEL (0x07)
  - \e ESC (0x1b)
  - \r CR (0x0d)
  - \v VT (0x0b)
  - \b BS (0x08)
  - \f FF (0x0c)
  - \s SP (0x20)
  - \uhhh 1–6 hex digits
  - \d DEL (0x7f)
  - \n NL (0x0a)
  - \t TAB (0x09)
  - \xhh 2 dígitos hex
- Permiten la interpolación con expresiones de elixir usando la sintaxis `#{}`
  ```elixir
    name = "MakingDevs"
    "Hello, #{String.capitalize name} !!!"
  ```
- Caracteres que tengan un significado especial pueden ser escapados.
- Cuentan con soporte *heredoc*.

### Sigils

Una lista de los sigils incorporados incluye:

- `~C` Genera una lista de caracteres sin escapado o interpolación
- `~c` Genera una lista de caracteres con escapado e interpolación
- `~R` Genera una expresión regular sin escapado o interpolación
- `~r` Genera una expresión regular con escapado e interpolación
- `~S` Genera una cadena sin escapado o interpolación
- `~s` Genera una cadena con escapado e interpolación
- `~W` Genera una lista de palabras sin escapado o interpolación
- `~w` Genera una lista de palabras con escapado e interpolación
- `~N` Genera una estructura NaiveDateTime

Una lista de delimitadores incluye:

- `<...>` Un par de simbolos de mayor/menor
- `{...}` Un par de llaves
- `[...]` Un par de corchetes
- `(...)` Un par de paréntesis
- `|...|` Un par de pipes
- `/.../` Un par de slashes
- `"..."` Un par de comillas dobles
- `'...'` Un par de comillas simples

### Strings con comilla simple, lista de códigos de cáracteres

Son representados como una lista de valores enteros, cada valor corresponde al _codepoint_ en el string.

```elixir
str = 'metallica'
is_list(str)
length str
Enum.reverse str
```

Una lista de enteros cuando se mande a pantalla se mostrará como un string, suponiendo que cada número en la lista es un carcater imprimible.

```elixir
str = [109, 97, 107, 105, 110, 103, 100, 101, 118, 115]
List.to_tuple str
str ++ [0]
```

Si la lista contiene carácteres que Erlang considera no imprimibles entonces obtendremos la representación de la lista. Por ejemplo: `'∂ƒ™¶§'`

Debido a que esto es una lista, más adelante veremos como usar _pattern matching_ y las funciones del módulo `List` para manipularla.

### Binarios

Este tipo representa una secuencia de bits. Un binario literal es algo como `<<term,...>>`. Los términos más simples son sólo números de 0 a 255, dichos números son almacenados cómo bytes sucesivos en el binario.

```elixir
b = << 1, 2, 3 >>
byte_size b # 3
bit_size b # 24
```

Se pueden específicar modificadores para establecer cualquier tamaño del término(en bits). Esto es útil cuando se trabaj con formatos binarios, archivos y paquetes de red, por ejemplo.

```elixir
b = <<1::size(2), 1::size(3)>> # 01 001
byte_size b # 1
bit_size b # 5
<< header :: size(8), data :: binary >> = <<1,2,3,4,5>>
```

[Por ejemplo puedes leer la estructura de un archivo PNG.](https://zohaib.me/binary-pattern-matching-in-elixir/)

### Strings binarios

El contenido de un string con doble comilla es almacenado como una secuencia de bytes en codificación UTF-8. Esto para hacerlo más eficiente en términos de memoria y de la forma en cómo se accesa.

Un caracter UTF-8 puede tomar más de un sólo byte para representarse, y dicho tamaño del binario no necesariamente es el tamaño del string.

```elixir
s = "ƒMD∫"
String.length s # 5
byte_size s # 10
String.at s, 0 # "ƒ"
String.codepoints s # ["ƒ", "M", "D", "", "∫"]
String.split s, "D" # ["ƒM", "∫"]
```

Debido a que no se están usando listas, necesitamos aprender a trabajar con la sintaxis de binarios.

```elixir
<<97, 98, 99, 100>> # "abcd"
s = "ƒMD∫"
<< c::bytes-size(2)>> <> rest = s
"Making" <> "Devs" # CONCAT!!!
```

Sugerimos ampliamente explorar el módulo [`String`](https://hexdocs.pm/elixir/String.html) de elixir, el cual, tiene funciones de conveniencia para el manejo de Strings binario.

---

El manejo de Strings en Elixir es el resultado de un proceso largo evolucionado en el ambiente de Erlang. Si comenzáramos desde el principio, las cosas propbablemente se verían un poco diferente. Pero una vez que lo comprendemos entonces la ligera extrañeza de que los strings coinciden usando binarios, encontraremos que funciona muy bien. Tendrás que profundizar en el tema de **Pattern Matching** para aprovechar y facilitar mucho el uso de Strings.
