# Procesamiento de colecciones

## Módulo `Enum`

La mayoría de las funciones en este módulo tienen esta firma:

```elixir
Enum.function(Enumerable, fun)
```

Los tipos que implementan el protocolo `Enumerable` son:

- Listas
- Keyword lists
- Mapas(excepto Structs)
- Rangos
- Streams

Lo importante aquí es conocer la estructura del [protocolo Enumerable](https://hexdocs.pm/elixir/Enumerable.html).

Realmente las funciones del módulo `Enum` son bastante explícitas, aunque siempre será recomendable explorarlas con la ayuda proveída por Elixir en la _iex_:

```bash
iex(1)> Enum.
EmptyError           OutOfBoundsError     all?/1
all?/2               any?/1               any?/2
at/2                 at/3                 chunk_by/2
chunk_every/2        chunk_every/3        chunk_every/4
chunk_while/4        concat/1             concat/2
count/1              count/2              dedup/1
dedup_by/2           drop/2               drop_every/2
drop_while/2         each/2               empty?/1
fetch!/2             fetch/2              filter/2
find/2               find/3               find_index/2
find_value/2         find_value/3         flat_map/2
flat_map_reduce/3    frequencies/1        frequencies_by/2
group_by/2           group_by/3           intersperse/2
into/2               into/3               join/1
join/2               map/2                map_every/3
map_intersperse/3    map_join/2           map_join/3
map_reduce/3         max/1                max/3
max_by/2             max_by/4             member?/2
min/1                min/3                min_by/2
min_by/4             min_max/1            min_max/2
min_max_by/2         min_max_by/3         random/1
reduce/2             reduce/3             reduce_while/3
reject/2             reverse/1            reverse/2
reverse_slice/3      scan/2               scan/3
shuffle/1            slice/2              slice/3
sort/1               sort/2               sort_by/2
sort_by/3            split/2              split_while/2
split_with/2         sum/1                take/2
take_every/2         take_random/2        take_while/2
to_list/1            uniq/1               uniq_by/2
unzip/1              with_index/1         with_index/2
zip/1                zip/2
```

### Tips al usar estas funciones

- Presta especial atención en aquellas funciones donde la aridad cambia.
- Puedes usar Pattern Matching
- Recuerda usar el operador de captura **&**.

## Streams

`Stream` es una versión _"perezosa"_ de `Enum`.

```elixir
[1, 2, 3, "string"]
|> Stream.filter(&is_number/1)
|> Stream.map(&(&1 * 2))
```

Esto provee de una estructura de datos como la siguiente:

```elixir
%Stream{
  enum: [1,2,3,"string"],
  funs: [
    #Function<...>,
    #Function<...>
  ]
}
```

Las operaciones de `Stream` pueden ser construidaas programaticamente.

```elixir
stream = Stream.filter(list, &is_number/1)
stream = Stream.filter(stream, &(&1 * 2 == 4))
Enum.into(stream, [])
```

Cuando no necesitas el resultado podrías ocupar `Stream.run/1`:

```elixir
[1,2,3]
|> Stream.each(fn n -> IO.puts(n) end)
|> Stream.run
```

### ¿`Stream` es mejor que `Enum`?

```elixir
# Itera sobre la lista dos veces
list
|> Enum.filter(&is_number/1)
|> Enum.filter(&(&1 * 2 == 4))

# Itera la lista una vez
list
|> Stream.filter(&is_number/1)
|> Stream.filter(&(&1 * 2 == 4))
|> Enum.into([])
```

### Capacidades de los Stream's

Puedes crear flujos infinitos de elementos.

```elixir
Stream.cycle(["Square", "Circle", "Rectangle"])
|> Enum.take(8)
```

```elixir
Stream.iterate(2, &(&1 * 2))
|> Enum.take(5)
```

> TODO: Revisa la documentación de `Stream.resource(start_fun, next_fun, after_fun)`

### Stream vs. Enum

- `Stream` implementa la mayoría de las funciones de `Enum`, de forma perezosa
- `Enum` es mejor para enumrables cortos y una operación
- `Stream` es mejor para enumerables largos con múltiples operaciones

## Comprehensiones

Los tres elementos de la macro `for` son los siguientes:

- Generadores
- Filtros
- `:into`

`for` combina características de `map`, `filter` e `into`.

```elixir
for element <- Enumerable do
  element
end
# Regresa una lista
```

### Generadores

Estos son de la forma `element <- Generator`, y un generador simple se comporta como un mapa:

```elixir
for name <- ["MakingDevs", "José Juan"] do
  String.upcase(name)
end
# => ["JOE", "SUZY"]

# Es equivalente a:
Enum.map ["MakingDevs", "José Juan"], fn(name) ->
  String.upcase(name)
end
```

Puedes usar múltiples generadores para crear listas complejas:

```elixir
for x <- [1, 2, 3], y <- [4, 5, 6], do: x * y
# [4, 5, 6, 8, 10, 12, 12, 15, 18]

for i <- [:a, :b, :c], j <- [1, 2], do:  {i, j}
# [a: 1, a: 2, b: 1, b: 2, c: 1, c: 2]
```

Los generadores pueden hacer filtrado con un patrón.

```elixir
a_list = [a: 1, a: 2, b: 1, b: 2, c: 1, c: 2]
for {:a, n} <- a_list do
  {:a, n}
end
```

### Filtros

Escribe los filtros después de los generadores:

```elixir
for element <- Enumerable, filter do
  element
end
```

Puedes usar las variables desde los generadores en los filtros:

```elixir
import Integer
for x <- 1..10, is_even(x), do: x
```

Inclusive puedes tener múltiples filtros:

```elixir
import Integer
for x <- 1..100,
  is_even(x),
  rem(x, 3) == 0, do: x
```

### `:into`

Usa `:into` para juntar los resultados en un `Collectable`.

```elixir
for {k, v} <- [one: 1, two: 2, three: 3], into: %{}, do: {k, v}

for item <- [:bananas, :milk],
  quantity <- [5, 2],
  into: Map.new,
  do: {item, quantity}
```

Los tipos que soportan `Collectable` son:

- Map
- List
- IO.Stream
- Bitstring(Binary)

### Diferentes formas de escribir `for`

```elixir
for name <- names, do: String.upcase(name)

for name <- names do
  String.upcase(name)
end
```

### Estructura general de la comprensión

**for <generator(s) or filter(s), separated by commas> [, into: <value>], do: <expression>**

- Todas las variables usadas en `for` son locales.
- Tips:
  - Una operación: `Enum` `for`
  - Múltiples operaciones: `for` o `Stream`
  - Generadores de listas: `for`o un generador de `Stream`
  - Múltiples listas: `for`

> TODO: Lee la documentación de `Collectable` y de `for`
