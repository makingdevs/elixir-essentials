# Herramientas de construcción

> **Mix** es una herramienta de construcción que provee tareas para crear, compilar y probar proyectos hechos con Elixir, administrar sus dependencias, y más.

Previo al uso de *mix*, veamos un par de formas en las que podemos ejecutar código de  Elixir.

## Ejecutando código de elixir

Suponiendo que cuentas con un archivo _hello.ex_ con el siguiente bloque:

```elixir
defmodule Hello do
  def greet(name \\ "you") do
  	"Hello #{name} !!!"
  end
end

IO.puts Hello.greet("MakingDevs")
```

### `elixir`

Puedes ejecutar el código anterior con el comando `elixir hello.ex`, y de manera secuencial leerá el archivo para después mostrar el resultado.

### `iex`

Puedes llamar al comando `iex hello.ex` para comenzar una shell interactiva(REPL), y cargar el código contenido para poderse usar de inmediato.

### Obteniendo ayuda

Una de las características que tienen el código y las bibliotecas de Elixir(así como las que tu puedes crear también), es que puedes obtener ayuda de cualquier módulo con la función `h`, por ejemplo:

```bash
iex(1)> h String

                                     String

A String in Elixir is a UTF-8 encoded binary.

## Codepoints and grapheme cluster

The functions in this module act according to the Unicode Standard, version
11.0.0.

As per the standard, a codepoint is a single Unicode Character, which may be
represented by one or more bytes.
```

Esto te será bastante útil cuando estés manipulando código y haciendo pruebas de concepto.

## Uso de `mix`

Para comenzar tendremos que crear un proyecto de mix, de la siguiente forma:

```bash
➜ mix new fifht_element
* creating README.md
* creating .formatter.exs
* creating .gitignore
* creating mix.exs
* creating config
* creating config/config.exs
* creating lib
* creating lib/fifht_element.ex
* creating test
* creating test/test_helper.exs
* creating test/fifht_element_test.exs

Your Mix project was created successfully.
You can use "mix" to compile it, test it, and more:

    cd fifht_element
    mix test

Run "mix help" for more commands.
```

Con lo cuál obtendrías una estructura de directorios como la siguiente:

```bash
➜ fifht_element tree
├── README.md
├── config
│   └── config.exs
├── lib
│   └── fifht_element.ex
├── mix.exs
└── test
    ├── fifht_element_test.exs
    └── test_helper.exs

3 directories, 6 files
```

## `mix tasks`

Todas las tareas que puedes ejecutar en **mix** están ligadas a un módulo, por lo tanto tu también podrías crear tus propias tareas, aunque no es tema de este primer módulo tienes que saber que no es tan díficil.

Veremos las más relevantes para esta primer parte de nuestro entrenamiento.

### `mix new`

Existen varias formas de crear un nuevo proyecto, las cuáles tienes que saber para en un futuro determinar que tipos de proyectos quieres crear. [Revisa la tarea en la documentación](https://hexdocs.pm/mix/Mix.Tasks.New.html).

### Mix.Project

La definición de un proyecto de mix se hace en un archivo *mix.exs*.

```elixir
defmodule FifhtElement.MixProject do
  use Mix.Project

  def project do
    [
      app: :fifht_element,
      version: "0.1.0",
      elixir: "~> 1.7",
    ]
  end

end
```

Con esto puedes correr algunas de las tareas más simples:

- [`mix compile`](https://hexdocs.pm/mix/Mix.Tasks.Compile.html)
- [`mix test`](https://hexdocs.pm/mix/Mix.Tasks.Test.html)
- [`mix run`](https://hexdocs.pm/mix/Mix.Tasks.Run.html)

Cada tarea tiene sus propias opciones y algunas veces configuración específica para ser definida en la función `project/0`.

### Dependencias

Mix además maneja las dependencias y las integra perfectamente con el [administrador de paquetes Hex](https://hex.pm).

```elixir
defmodule FifhtElement.MixProject do
  use Mix.Project

    def project do
    [
      app: :fifht_element,
      version: "0.1.0",
      elixir: "~> 1.7",
      deps: deps()
    ]
  end

  defp deps do
    [
      {:ecto, "~> 2.0"},
      {:plug, github: "elixir-lang/plug"}
    ]
  end

end
```

Te recomiendo explorar con `mix help deps` las posibilidades que tienes para administrar las dependencias.

Una vez que ya tienes tus dependencias definidas sólo tienes que ejecutar `mix deps.get`:

```bash
➜ mix deps.get
* Getting plug (https://github.com/elixir-lang/plug.git)
remote: Enumerating objects: 105, done.
remote: Counting objects: 100% (105/105), done.
remote: Compressing objects: 100% (68/68), done.
remote: Total 9339 (delta 46), reused 74 (delta 35), pack-reused 9234
Receiving objects: 100% (9339/9339), 2.22 MiB | 1.42 MiB/s, done.
Resolving deltas: 100% (5215/5215), done.
Resolving Hex dependencies...
Dependency resolution completed:
New:
  decimal 1.8.0
  ecto 2.2.12
  mime 1.3.1
  plug_crypto 1.0.0
  poolboy 1.5.2
* Getting ecto (Hex package)
* Getting mime (Hex package)
* Getting plug_crypto (Hex package)
* Getting decimal (Hex package)
* Getting poolboy (Hex package)
```

> Es hora de ejecutar la `iex` con mix para lo cuál puedes ejecutar lo siguiente:

```bash
➜ iex -S mix
Erlang/OTP 21 [erts-10.2] [source] [64-bit] [smp:4:4] [ds:4:4:10] [async-threads:1] [hipe]

Interactive Elixir (1.7.4) - press Ctrl+C to exit (type h() ENTER for help)
iex(1)>
```

Ya puedes usar módulos de las dependencias que definiste.

Más información al respecto del uso de `mix` la puedes encontrar en la [documentación de Hex](https://hex.pm/docs/usage).

### Ambientes

Mix soporta diferentes ambientes. Por default, Mix provee tres ambientes:

- `:dev` Ambiente por default
- `:test` El ambiente que se activa con `mix test`
- `:prod` El ambiente dónde corren las dependencias

El entorno puede ser cambiado con la variable de entorno `MIX_ENV`.

```bash
➜ MIX_ENV=prod mix run server.exs
```

Dicho entorno puede ser leído con la función `Mix.env/0`.

### Alias

Son caminos cortos o tareas específicos para el proyecto actual. Usamos el key `aliases` para definir con un _keyword list_ los alias.

```elixir
defmodule FifhtElement.MixProject do
  use Mix.Project

    def project do
    [
      app: :fifht_element,
      version: "0.1.0",
      elixir: "~> 1.7",
      aliases: aliases()
    ]
  end

  defp aliases do
    [
      c: "compile",
      hello: &hello/1,
      all: [&hello/1, "deps.get --only #{Mix.env()}", "compile"]
    ]
  end

  defp hello(_) do
    Mix.shell().info("Hello world")
  end

end
```

## Archivos _.ex_ y _.exs_

`.ex` es para código compilado, `.exs` para código interpretado.

Las pruebas de ExUnit por ejemplo están `.exs`, de tal manera que no tienes que recompilar cada vez que haces un cambio. Si usas scripts o pruebas deberías de usar `.exs`. De otra forma, sólo usa archivos `.ex` y compila tu código.

Cómo pros y contras, la interpretación tomará más tiempo para ejecutar, pero no requiere compilación para correr. Eso es demasiado si la flexibilidad de correr scripts es más importante que el tiempo de ejecución optimizado entonces usa archivos `.exs`. La mayoría del tiempo usarás `.ex`.

En Elixir 1.9.1, ambos son compilados para optimizar su ejecución, sin embargo existen elementos de implementación que los diferencian.